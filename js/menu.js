$('li').click(function() {
  $('#contact').toggleClass('animate-1')
  $('#about').toggleClass('animate-2')
  $('#home').toggleClass('animate-3')
})

var yesSlick = $('.yes-slick').length

if (yesSlick) {
	$('.slider').slick({ 
		dots: false, 
		arrows: true, 
		infinite: true, 
		slidesToShow: 1, 
		slidesToScroll: 1,
		fade: true,
		prevArrow: '<div class="slick-prev"></div>',
		nextArrow: '<div class="slick-next"></div>',
		autoplay: true,
		autoplaySpeed: 3000,
		speed: 1000
	}); 

	$('.progress-bar').addClass('active');



	$('.slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
		$('.progress-bar').removeClass('active');
	});

	$('.slider').on('afterChange', function(event, slick, currentSlide, nextSlide){
		$('.progress-bar').addClass('active');
	});
}

 function toggleMenu() {
 	$('.mobile-menu').toggleClass("open");
 	console.log('clicked')
 }

